package com.gosoft.shopdemo;

import java.io.Serializable;

public class ShopId implements Serializable {
	private static final long serialVersionUID = 1L;
	
	private int shopId;

	public ShopId() {
		super();
		// TODO Auto-generated constructor stub
	}

	public int getShopId() {
		return shopId;
	}

	public void setShopId(int shopId) {
		this.shopId = shopId;
	}
	

}
