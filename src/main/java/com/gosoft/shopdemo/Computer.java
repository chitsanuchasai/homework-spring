package com.gosoft.shopdemo;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;

@Entity
public class Computer {
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private Integer id;

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}
	
	private String brand;
	private String cpu;
	private String ram;
	private String hhd;

	public String getBrand() {
		return brand;
	}

	public void setBrand(String brand) {
		this.brand = brand;
	}

	public String getCpu() {
		return cpu;
	}

	public void setCpu(String cpu) {
		this.cpu = cpu;
	}

	public String getRam() {
		return ram;
	}

	public void setRam(String ram) {
		this.ram = ram;
	}

	public String getHhd() {
		return hhd;
	}

	public void setHhd(String hhd) {
		this.hhd = hhd;
	}
	
	@ManyToOne
	@JoinColumn(name = "shop_id")
	ShopDTO shop;

	public ShopDTO getShop() {
		return shop;
	}

	public void setShop(ShopDTO shop) {
		this.shop = shop;
	}

	@Override
	public String toString() {
		return "[id=" + id + ", brand=" + brand + ", cpu=" + cpu + ", ram=" + ram + ", hhd=" + hhd + ", shop="
				+ shop + "]";
	}
	
	
}
