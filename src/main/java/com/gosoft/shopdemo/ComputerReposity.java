package com.gosoft.shopdemo;

import java.util.List;

import org.springframework.data.repository.CrudRepository;

public interface ComputerReposity extends CrudRepository<Computer, Integer>{
	
	List<Computer> findByshop(ShopDTO shop);


}
