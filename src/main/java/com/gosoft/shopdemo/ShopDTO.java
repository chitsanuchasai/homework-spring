package com.gosoft.shopdemo;

import java.io.Serializable;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.IdClass;
import javax.persistence.JoinColumn;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.Table;


@Entity
@Table(name="Shop")
@IdClass(ShopId.class)
public class ShopDTO implements Serializable {
	
	private String name;
	private String address;
	@Id
	@Column(name = "shop_id", unique = true)
	private int shopId;

	public int getShopId() {
		return shopId;
	}

	public void setShopId(int shopId) {
		this.shopId = shopId;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getAddress() {
		return address;
	}

	public void setAddress(String address) {
		this.address = address;
	}

	@OneToOne(cascade = CascadeType.ALL)
	@JoinColumn(name = "manager_id", referencedColumnName = "id")
	private Manager manager;

	@OneToMany(mappedBy = "shop")
	private Set<Staff> staffs;
	
	@OneToMany(mappedBy = "shop")
	private Set<Computer> computers;

	public Manager getManager() {
		return manager;
	}

	public void setManager(Manager manager) {
		this.manager = manager;
	}

	public Set<Staff> getStaffs() {
		return staffs;
	}

	public void setStaffs(Set<Staff> staffs) {
		this.staffs = staffs;
	}

	public Set<Computer> getComputers() {
		return computers;
	}

	public void setComputers(Set<Computer> computers) {
		this.computers = computers;
	}

	@Override
	public String toString() {
		return "ShopDTO [name=" + name + ", address=" + address + ", shopId=" + shopId + ", manager=" + manager
				+ ", staffs=" + staffs.toString() + ", computers=" + computers + "]";
	}
	
	
}
