package com.gosoft.shopdemo;

import java.util.List;

import org.springframework.data.repository.CrudRepository;

public interface StaffRepository extends CrudRepository<Staff, Integer>{
	List<Staff> findByshop(ShopDTO shop);
}
