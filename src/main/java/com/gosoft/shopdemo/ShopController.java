package com.gosoft.shopdemo;


import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;
import java.util.stream.Collectors;


@RestController
@Component
public class ShopController {
	@Autowired
	ShopRepository shopRepo;
	@Autowired
	StaffRepository staff;
	@Autowired
	ComputerReposity computer;
	@Autowired
	ManagerRepository manager;
//	test
	@PostMapping(path = "/shop", consumes = { MediaType.APPLICATION_JSON_VALUE })
	@ResponseBody
	String newShopData(@RequestBody ShopDTO shop) {
		StringBuilder sb = new StringBuilder();
		
		shopRepo.save(shop);
		sb.append(" Shop is Save!!  \nshop name ->" + shop.getName());
		sb.append(" \nShop is Save!! : shop adddress ->" + shop.getAddress());
		
		if (shop.getManager() != null) {
			manager.save(shop.getManager());
			sb.append(" \nManager nmae ->" + shop.getManager().getName() +" "+ shop.getManager().getLastName());
		}

		if (shop.getComputers() != null) {
			if (!shop.getComputers().isEmpty()) {
				for (Computer c : shop.getComputers()) {
					c.setShop(shop);
					computer.save(c);
					sb.append(" \nComputer brand ->" + c.getBrand());
				}
			}
		}

		if (shop.getStaffs() != null ) {
			if (!shop.getStaffs().isEmpty()) {
				for (Staff t : shop.getStaffs()) {
					t.setShop(shop);
					staff.save(t);
					sb.append(" \nStaff name ->" + t.getName());
				}
			}

		}
		
		return sb.toString() ;
	}
	
	@GetMapping("/getshop")
	@ResponseBody
	String  getShopData() {
		StringBuilder sb = new StringBuilder();
		Iterable<ShopDTO> shopList = shopRepo.findAll();
		for (ShopDTO s : shopList) {
			sb.append(" Shop name: " + s.getName());
			sb.append(" \nShop adress: " + s.getAddress());
			sb.append(" \nManager: " + s.getManager());
			if (!s.getStaffs().isEmpty()) {
				s.getStaffs().stream().sorted();
				sb.append("\nStaff: ");
				sb.append(s.getStaffs().stream().map(e -> e.toString()).collect(Collectors.joining(", ")));
			}
			if (!s.getComputers().isEmpty()) {
				s.getComputers().stream().sorted();
				sb.append(" \nComputer: " + s.getComputers().stream().map(e -> e.toString()).collect(Collectors.joining(", ")));
			}
			sb.append(" \n");
			sb.append(" \n");
		}
		return sb.toString();
	}
	
	
	@PutMapping(path = "/computer/{computer_id}", consumes = { MediaType.APPLICATION_JSON_VALUE })
	@ResponseBody
	String updateComputer(@PathVariable("computer_id") int computer_id, @RequestBody Computer computer_input) {
		Computer c = computer.findById(computer_id).get();
		if (c != null) {
			c.setBrand(computer_input.getBrand());
			c.setCpu(computer_input.getCpu());
			c.setHhd(computer_input.getHhd());
			c.setRam(computer_input.getRam());
			computer.save(c);
			return "Update computer id  " + computer_id;
		} else {
			return "Cannot found computer id";
		}
	}
	
	@GetMapping(path = "/shopRemove/{shop_id}")
	@ResponseBody
	String shopRemoveComputer(@PathVariable("shop_id") int shop_id, @RequestParam("computer_id") Integer computer_id, @RequestParam("staff_id") Integer staff_id, @RequestParam("manager_id") Integer manager_id) {
		ShopDTO shopDto = shopRepo.findByShopId(shop_id);
		StringBuilder sb = new StringBuilder();
		if (computer_id != 0) {
			List<Computer> c = computer.findByshop(shopDto);
			if (!c.isEmpty()) {
				for (Computer computerDB : c) {
					if (computerDB.getId() == computer_id) {
						sb.append("\nshop remove computer: " + computer_id);
						computerDB.setShop(null);
						computer.save(computerDB);
					}
				}
			}
		}
		
		if (staff_id != 0) {
			List<Staff> staffList = staff.findByshop(shopDto);
			if (!staffList.isEmpty()) {
				for (Staff staffDB: staffList) {
					if (staffDB.getId() == staff_id) {
						sb.append("\nshop remove staff: " + staff_id);
						staffDB.setShop(null);
						staff.save(staffDB);
					}
				}
			}
		}
		
		if (manager_id != 0) {
			if (manager_id == shopDto.getManager().getId()) {
				sb.append("\nshop remove manager: " + manager_id);
				shopDto.setManager(null);
				shopRepo.save(shopDto);
			}
		}
		return sb.toString();
	}
	
	@PutMapping(path = "/staff/{staff_id}", consumes = { MediaType.APPLICATION_JSON_VALUE })
	@ResponseBody
	String updateStaff(@PathVariable("staff_id") int staff_id, @RequestBody Staff staff_input) {
		Staff s = staff.findById(staff_id).get();
		if (s != null) {
			s.setName(staff_input.getName());
			s.setLastName(staff_input.getLastName());
			s.setSalary(staff_input.getSalary());
			staff.save(s);
			return "Update Staff id  " + staff_id;
		} else {
			return "Cannot found Staff id";
		}
	}
	
	@PutMapping(path = "/manager/{manager_id}", consumes = { MediaType.APPLICATION_JSON_VALUE })
	@ResponseBody
	String updateManager(@PathVariable("manager_id") int manager_id, @RequestBody Manager manager_input) {
		Manager m = manager.findById(manager_id).get();
		if (m != null) {
			m.setName(manager_input.getName());
			m.setLastName(manager_input.getLastName());
			m.setSalary(manager_input.getSalary());
			manager.save(m);
			return "Update Manager id  " + manager_id;
		} else {
			return "Cannot found Manager id";
		}
	}
	
	@DeleteMapping("/staff/{staff_id}")
	@ResponseBody
	String deleteStaff(@PathVariable("staff_id") int staff_id) {
		Staff s = staff.findById(staff_id).get();
		staff.delete(s);
		return "Delete staff id " + s;
	}
	
	@DeleteMapping("/computer/{computer_id}")
	@ResponseBody
	String deleteComputer(@PathVariable("computer_id") int computer_id) {
		Computer c = computer.findById(computer_id).get();
		computer.delete(c);
		return "Delete Computer id " + c;
	}
	
	@DeleteMapping("/manager/{manger_id}")
	@ResponseBody
	String deleteManager(@PathVariable("manger_id") int manger_id) {
		Manager m = manager.findById(manger_id).get();
		Iterable<ShopDTO> shopList = shopRepo.findAll();
		for (ShopDTO shop : shopList) {
			if (shop.getManager().getId() == m.getId()) {
				shop.setManager(null);
				shopRepo.save(shop);
			}
		}
		manager.delete(m);
		return "Delete manger id " + m;
	}

	
	
}
