package com.gosoft.shopdemo;

import org.springframework.data.repository.CrudRepository;

public interface ShopRepository extends CrudRepository<ShopDTO, Integer>{
	ShopDTO findByShopId(int shop_id);

}
